window.onload = () => {
    const audioContext = new AudioContext();
    const metronome = new Metronome(audioContext);
    // metronome.ontick.subscribe((e) => { console.info("Tick #" + e.tickNumber); });

    let startMetronome = () => {
        let bpm = (document.getElementById("txtBpm") as HTMLInputElement).valueAsNumber;
        let ticksPerMeasure = (document.getElementById("txtTicksPerMeasure") as HTMLInputElement).valueAsNumber;
        let accentFirstTick = (document.getElementById("chckBoxAccentFirstTick") as HTMLInputElement).checked;

        metronome.start(bpm, ticksPerMeasure, accentFirstTick);
    };

    document.getElementById("btStart").onclick = () => startMetronome();
    document.getElementById("btStop").onclick = () => metronome.stop();
    document.getElementById("chckBoxAccentFirstTick").onchange = () => startMetronome();
    document.getElementById("txtBpm").onchange = () => startMetronome();
};
