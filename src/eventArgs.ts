class EventArgs {
    // workaround for causing compiler errors when other type doesn't extend empty class EventArgs:
    // https://github.com/Microsoft/TypeScript/issues/202
    private nominalValue: number;

    // tslint:disable-next-line:no-unused-expression
    constructor() { this.nominalValue; }

    public static get empty() { return new EventArgs(); }
}
