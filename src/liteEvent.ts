interface ILiteEvent<T extends EventArgs> {
    subscribe(handler: (eventArgs: T) => void): void;
    unsubscribe(handler: (eventArgs: T) => void): void;
}

class LiteEvent<T extends EventArgs> implements ILiteEvent<T> {
    private handlers: Array<(eventArgs: T) => void> = [];

    public subscribe(handler: (eventArgs: T) => void): void {
        this.handlers.push(handler);
    }

    public unsubscribe(handler: (eventArgs: T) => void): void {
        this.handlers = this.handlers.filter((h) => h !== handler);
    }

    public invoke(eventArgs: T) {
        this.handlers.slice(0).forEach((h) => h(eventArgs));
    }
}
