class Metronome {
    private static readonly lowSoundPath: string = "assets/audio/low_woodblock.wav";
    private static readonly highSoundPath: string = "assets/audio/high_woodblock.wav";
    private static lowSoundBuffer: AudioBuffer;
    private static highSoundBuffer: AudioBuffer;

    private readonly tickEvent = new LiteEvent<MetronomeTickEventArgs>();
    private readonly context: AudioContext;
    private intervalSeconds: number;
    private ticksPerMeasure: number;
    private accentFirstTick: boolean;
    private audioSource: AudioBufferSourceNode;
    private running: boolean;
    private elapsedTicksPerMeasure: number = 0;

    constructor(audioContext: AudioContext) {
        this.context = audioContext;
    }

    public get ontick() { return this.tickEvent as ILiteEvent<MetronomeTickEventArgs>; }

    public start(bpm: number, ticksPerMeasure: number, accentFirstTick: boolean) {
        this.stop();

        this.intervalSeconds = 60 / bpm;
        this.ticksPerMeasure = ticksPerMeasure;
        this.accentFirstTick = accentFirstTick;

        if (Metronome.lowSoundBuffer != null && Metronome.highSoundBuffer != null) {
            this.startTicking();
            return;
        }

        this.loadAudioBuffer(Metronome.lowSoundPath, (buffer) => {
            Metronome.lowSoundBuffer = buffer;
            if (Metronome.highSoundBuffer != null) {
                this.startTicking();
            }
        });
        this.loadAudioBuffer(Metronome.highSoundPath, (buffer) => {
            Metronome.highSoundBuffer = buffer;
            if (Metronome.lowSoundBuffer != null) {
                this.startTicking();
            }
        });
    }

    public stop() {
        if (this.running) {
            this.audioSource.stop();
            this.audioSource.disconnect(this.context.destination);
            this.running = false;
            this.elapsedTicksPerMeasure = 0;
        }
    }

    private loadAudioBuffer(path: string, loadedCallback: DecodeSuccessCallback) {
        let soundRequest = new XMLHttpRequest();
        soundRequest.open("GET", path, true);
        soundRequest.responseType = "arraybuffer";

        soundRequest.onload = () => {
            let audioData = soundRequest.response;

            this.context.decodeAudioData(audioData, loadedCallback,
                (ex) => {
                    console.error("Error while trying to decode audio data: " + ex.message);
                });
        };

        soundRequest.send();
    }

    private startTicking(when?: number) {
        let startTime = when || this.context.currentTime;
        this.running = true;

        let buffer = this.accentFirstTick ? Metronome.highSoundBuffer : Metronome.lowSoundBuffer;
        this.audioSource = this.initializeAudioSource(buffer, startTime + this.intervalSeconds);
        this.audioSource.start(startTime);
    }

    private initializeAudioSource(buffer: AudioBuffer, nextStart: number): AudioBufferSourceNode {

        let newAudioSource = this.context.createBufferSource();
        newAudioSource.onended = () => {
            ++this.elapsedTicksPerMeasure;
            this.tickEvent.invoke(new MetronomeTickEventArgs(this.elapsedTicksPerMeasure));

            this.elapsedTicksPerMeasure = this.elapsedTicksPerMeasure === this.ticksPerMeasure
                ? 0
                : this.elapsedTicksPerMeasure;
            this.schedule(nextStart);
        };
        newAudioSource.connect(this.context.destination);
        newAudioSource.buffer = buffer;

        return newAudioSource;
    }

    private schedule(nextStart: number) {
        this.audioSource.stop();
        this.audioSource.disconnect(this.context.destination);

        let buffer = this.accentFirstTick && this.elapsedTicksPerMeasure === 0
            ? Metronome.highSoundBuffer
            : Metronome.lowSoundBuffer;
        this.audioSource = this.initializeAudioSource(buffer, nextStart + this.intervalSeconds);
        this.audioSource.start(nextStart);
    }
}
