class MetronomeTickEventArgs extends EventArgs {
    constructor(private readonly tickNum: number) {
        super();
    }

    public get tickNumber() { return this.tickNum; }
}
